from matplotlib import pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
import CFDClasses


results = CFDClasses.ReadOutputFile("smaller.txt")

fig, ax = plt.subplots(1,1)
plt.subplots_adjust(left=0.15, bottom=0.15)
Q = ax.quiver(results.x_mesh, results.y_mesh, results.ux_mesh(0), results.uy_mesh(0), pivot='mid', color='r', units='inches')

plt.title("Lid driven cavity velocity vector plot")
plt.xlabel('x (m)')
plt.ylabel('y (m)')
ax_t = plt.axes([0.1, 0.01, 0.75, 0.03], facecolor='lightgoldenrodyellow')

t_slider = Slider(ax_t, 't (s)', results.t[0], results.t[-1], valinit=results.t[0])


def get_index(arr, val):
    for i in range(len(arr)):
        if arr[i] >= val:
            return i

def update(val):
    t = t_slider.val
    index = get_index(results.t, t)
    Q.set_UVC(results.ux_mesh(index), results.uy_mesh(index))
    fig.canvas.draw_idle()


t_slider.on_changed(update)

plt.show()
#
# def update_quiver(num, Q, X, Y):
#     """updates the horizontal and vertical vector components by a
#     fixed increment on each frame
#     """
#
#     U = np.cos(X + num*0.1)
#     V = np.sin(Y + num*0.1)
#
#     Q.set_UVC(U,V)
#
#     return Q,
#
# # you need to set blit=False, or the first set of arrows never gets
# # cleared on subsequent frames
# anim = animation.FuncAnimation(fig, update_quiver, fargs=(Q, X, Y),
#                                interval=50, blit=False)
# fig.tight_layout()
# plt.show()