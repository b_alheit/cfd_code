import CFDClasses
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons

results = CFDClasses.ReadOutputFile("Test_output.txt")
# plt.quiver(results.x, results.y, results.ux[-1, :], results.uy[-1, :])
# plt.show()

fig, ax = plt.subplots()

p = ax.quiver(results.x, results.y, results.ux[-1, :], results.uy[-1, :])

plt.axis([0, 1, -10, 10])

ax_t = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor='lightgoldenrodyellow')

t_slider = Slider(ax_t, 't (s)', results.t[0], results.t[-1], valinit=results.t[0])

def get_index(arr, val):
    for i in range(len(arr)):
        if arr[i] >= val:
            return i

def update(val):
    t = t_slider.val
    index = get_index(results.t, t)
    p.set_UVC(results.ux[index, :], results.uy[index, :])
    fig.canvas.draw_idle()


t_slider.on_changed(update)

plt.show()