import numpy as np
import matplotlib.pyplot as plt

class CFDSimulation:
    def __init__(self, rho, mu, p_ref, mesh, e, CFL=0.9, dt_write=None):
        print("Initiating simulation")
        self.mesh = mesh
        self.p_ref = p_ref
        self.rho = rho
        self.mu = mu
        self.dt = 0
        self.CFL = CFL
        self.e = e
        self.converged = False

        self.n_nodes = mesh.n_nodes
        self.u_next = np.zeros([self.n_nodes, 2])
        self.apply_BCs()
        self.u = np.copy(self.u_next)
        self.ws = np.zeros([self.n_nodes, 2])
        self.del_p = np.zeros([self.n_nodes, 2])
        self.diffuse = np.zeros([self.n_nodes, 2])
        self.convec = np.zeros([self.n_nodes, 2])
        self.p = np.zeros(self.n_nodes)
        self.u_mag = np.zeros(self.n_nodes)

        self.t_write = np.array([0])
        self.ux_write = np.copy([self.u_next[:, 0]])
        self.uy_write = np.copy([self.u_next[:, 1]])
        self.p_write = np.copy([self.p])
        self.dt_write = dt_write

        if dt_write is None:
            self.calc_dt()
            self.dt_write = self.dt * 3

        self.b = np.zeros(self.n_nodes)
        self.geometric_matrix = np.zeros([self.n_nodes, self.n_nodes])
        print("    Creating pressure matrix")
        for i in range(self.n_nodes):
            sum = 0
            for j in range(4):
                if mesh.NSEW[i, j] is not None:
                    sum -= mesh.A_NSEW[i, j]/mesh.t_NSEW[i, j]
            for j in range(5):
                if j == 0:
                    self.geometric_matrix[i, i] = sum
                elif mesh.NSEW[i, j-1] is not None:
                    self.geometric_matrix[i, mesh.NSEW[i, j-1]] = mesh.A_NSEW[i, j-1] / mesh.t_NSEW[i, j-1]
        self.geometric_matrix[0, :] = 0
        self.geometric_matrix[0, 0] = 1
        print("    Inverting pressure matrix")
        self.geometric_matrix = np.linalg.inv(self.geometric_matrix)

    def solve(self):
        print("    Solving simulation")
        it = 0
        t = 0
        while not self.converged:
            self.u = np.copy(self.u_next)
            self.calc_dt()
            t += self.dt

            self.calc_ws()
            self.calc_p()
            self.calc_u_next()

            self.apply_BCs()
            self.check_convergence()
            if t >= self.t_write[-1] + self.dt_write:
                self.t_write = np.append(self.t_write, t)
                self.ux_write = np.append(self.ux_write, [self.u_next[:, 0]], axis=0)
                self.uy_write = np.append(self.uy_write, [self.u_next[:, 1]], axis=0)
                self.p_write = np.append(self.p_write, [self.p], axis=0)

            it += 1
        self.u_mag = np.linalg.norm(self.u_next, axis=1)
        print("Simulation solved")

    def write_output(self, name):
        print("Writing output")
        file = open(name, 'w')
        header = name + """\n
rho = """ + str(self.rho) + """
mu = """ + str(self.mu) + """
p_ref = """ + str(self.p_ref) + """
CFL = """ + str(self.CFL) + """
e = """ + str(self.e) + "\n\nSTART\n"
        file.write(header)
        file.close()

        file = open(name, 'a')
        np.savetxt(file, self.t_write, newline=" ")
        file.write("\n")
        np.savetxt(file, self.mesh.pos[:, 0], newline=" ")
        file.write("\n")
        np.savetxt(file, self.mesh.pos[:, 1], newline=" ")
        file.write("\n")
        np.savetxt(file, self.ux_write)
        np.savetxt(file, self.uy_write)
        np.savetxt(file, self.p_write)
        file.close()
        print("Output file written")


    def apply_BCs(self):
        self.u_next[self.mesh.N] = [1, 0]
        self.u_next[self.mesh.S + self.mesh.W + self.mesh.E] = [0, 0]

    def calc_dt(self):
        for i in range(self.n_nodes):
            u_diff = np.linalg.norm(self.u[i, :]) + (self.mu / (self.rho * 0.45 * self.mesh.dx_eff))
            dt = self.CFL*self.mesh.dx_eff/u_diff
            if i == 0 or dt < self.dt:
                self.dt = dt

    def calc_ws(self):
        self.ws[:, :] = 0
        self.calc_convective()
        self.calc_diffusive()
        self.ws = (-self.convec + self.diffuse) * self.dt

    def calc_convective(self):
        """
        Changes for anisotropic mesh:
            * u_cd no longer average value. Must be linearly interpolated

        :return:
        """
        self.convec[:, :] = 0
        for i in range(self.n_nodes):
            for j in range(2):

                if None not in self.mesh.NSEW[i, :]:
                    for k in range(4):
                        u_cd = 0.5*(self.u[i, :] + self.u[self.mesh.NSEW[i, k], :])
                        if np.dot(u_cd, self.mesh.n_NSEW[i, k]) >= 0:
                            u_up = self.u[i, :]
                        else:
                            u_up = self.u[self.mesh.NSEW[i, k], :]
                        self.convec[i, j] += (1/self.mesh.V[i]) * (self.rho*u_up[j]*np.dot(u_cd, self.mesh.n_NSEW[i, k, :]) * self.mesh.A_NSEW[i, k])
                else:
                    break

    def calc_diffusive(self):
        self.diffuse[:, :] = 0
        for i in range(self.n_nodes):
            for j in range(2):
                for k in range(4):
                    if self.mesh.NSEW[i, k] is not None:
                        del_u = (self.u[self.mesh.NSEW[i, k], j] - self.u[i, j]) / self.mesh.t_NSEW[i, k]
                        self.diffuse[i, j] += (self.mu/self.mesh.V[i]) * (del_u * self.mesh.A_NSEW[i, k])

    def calc_p(self):
        self.calc_b()
        self.p = np.matmul(self.geometric_matrix, self.b)

    def calc_b(self):
        self.b[:] = 0
        for i in range(self.n_nodes):
            for k in range(4):
                if self.mesh.NSEW[i, k] is not None:
                    u_cd = 0.5*(self.u[i, :] + self.u[self.mesh.NSEW[i, k], :])
                    ws_cd = 0.5*(self.ws[i, :] + self.ws[self.mesh.NSEW[i, k], :])
                    self.b[i] += self.mesh.A_NSEW[i, k] * np.dot(u_cd+(1/self.rho)*ws_cd, self.mesh.n_NSEW[i, k])
        self.b *= self.rho/self.dt
        self.b[0] = self.p_ref

    def calc_u_next(self):
        self.calc_del_p()
        self.u_next = self.u + (1/self.rho) * (-self.dt * self.del_p + self.ws)

    def calc_del_p(self):
        self.del_p[:, :] = 0
        for i in range(self.n_nodes):
            for j in range(2):
                for k in range(4):
                    if self.mesh.NSEW[i, k] is not None:
                        p_cd = 0.5*(self.p[i] + self.p[self.mesh.NSEW[i, k]])
                    else:
                        p_cd = self.p[i]
                    self.del_p[i, j] += (1/self.mesh.V[i]) * self.mesh.n_NSEW[i, k, j] * self.mesh.A_NSEW[i, k] * p_cd

    def check_convergence(self):
        error = np.linalg.norm(self.u_next - self.u)
        print("Percentage error :", 100 * self.e / error)
        self.converged = error < self.e


class ReadOutputFile:
    def __init__(self, file_name):
        file = open(file_name, 'r')
        file_str = file.read(-1)
        start = file_str.find("START")
        file_str = file_str[start+6:]
        self.t = np.fromstring(file_str[:file_str.find("\n")], dtype=float, sep=" ")
        self.n_steps = len(self.t)
        file_str = file_str[file_str.find("\n")+1:]
        self.x = np.fromstring(file_str[:file_str.find("\n")], dtype=float, sep=" ")
        file_str = file_str[file_str.find("\n")+1:]
        self.y = np.fromstring(file_str[:file_str.find("\n")], dtype=float, sep=" ")
        file_str = file_str[file_str.find("\n")+1:]
        self.n_nodes = len(self.x)

        rest = np.fromstring(file_str, sep=" ").reshape([3*self.n_steps, self.n_nodes])
        self.ux = rest[:self.n_steps, :]
        self.uy = rest[self.n_steps:2*self.n_steps, :]
        self.p = rest[2*self.n_steps:3*self.n_steps, :]

        self.nx = 0
        x = self.x[0]
        while x == self.x[self.nx]:
            self.nx += 1

        self.nx = int(self.nx)
        self.ny = int(len(self.x)/self.nx)

        self.x_mesh = self.x.reshape([self.ny, self.nx])
        self.y_mesh = self.y.reshape([self.ny, self.nx])

    def ux_mesh(self, i):
        return self.ux[i, :].reshape([self.ny, self.nx])

    def uy_mesh(self, i):
        return self.uy[i, :].reshape([self.ny, self.nx])

    def p_mesh(self, i):
        return self.p[i, :].reshape([self.ny, self.nx])

    def u_mag(self, i):
        return np.sqrt(np.power(self.ux_mesh(i), 2) + np.power(self.uy_mesh(i), 2))
    def u_mag_av(self, i):
        return (self.u_mag(i)[1:, 1:] + self.u_mag(i)[:-1, :-1])/2

    def u_dir(self, i):
        return np.divide(self.ux_mesh(i), self.u_mag(i)), np.divide(self.uy_mesh(i), self.u_mag(i))


class Mesh:
    def __init__(self, x_min, x_max, y_min, y_max, nx, ny):
        print("Initiating mesh")
        self.n_nodes = nx * ny
        self.pos = np.zeros([self.n_nodes, 2], dtype=float)
        self.V = np.zeros(self.n_nodes, dtype=float)
        self.NSEW = np.array([[None] * 4] * self.n_nodes)
        self.t_NSEW = np.array([[None] * 4] * self.n_nodes)
        self.A_NSEW = np.array([[None] * 4] * self.n_nodes)
        self.n_NSEW = np.array([[[None] * 2] * 4] * self.n_nodes)
        self.N = []
        self.S = []
        self.E = []
        self.W = []

        dx = (x_max-x_min) / (nx-1)
        dy = (y_max-y_min) / (ny-1)
        self.dx_eff = (1/((1/dx**2)+(1/dy**2)))**0.5

        node = 0

        for i in range(nx):
            for j in range(ny):
                self.pos[node, 0] = x_min + i * (x_max - x_min) / (nx - 1)
                self.pos[node, 1] = y_min + j * (y_max - y_min) / (ny - 1)
                self.n_NSEW[node, :, :] = [[0, 1],
                                           [0, -1],
                                           [1,0],
                                           [-1, 0]
                                           ]
                if i==0 and j==0:
                    self.S.append(node)
                    self.V[node] = dx*dy/4
                    self.NSEW[node, :] = [node+1, None, node+ny, None]
                    self.t_NSEW[node, :] = [dy, None, dx, None]
                    self.A_NSEW[node, :] = [dx/2, dx/2, dy/2, dy/2]

                elif i==nx-1 and j==0:
                    self.S.append(node)
                    self.V[node] = dx*dy/4
                    self.NSEW[node, :] = [node+1, None, None, node-ny]
                    self.t_NSEW[node, :] = [dy, None, None, dx]
                    self.A_NSEW[node, :] = [dx/2, dx/2, dy/2, dy/2]

                elif i==nx-1 and j==ny-1:
                    self.E.append(node)
                    self.V[node] = dx*dy/4
                    self.NSEW[node, :] = [None, node-1, None, node-ny]
                    self.t_NSEW[node, :] = [None, dy , None, dx]
                    self.A_NSEW[node, :] = [dx/2, dx/2, dy/2, dy/2]

                elif i==0 and j==ny-1:
                    self.W.append(node)
                    self.V[node] = dx*dy/4
                    self.NSEW[node, :] = [None, node-1, node+ny, None]
                    self.t_NSEW[node, :] = [None, dy , dx, None]
                    self.A_NSEW[node, :] = [dx/2, dx/2, dy/2, dy/2]

                elif i==0:
                    self.W.append(node)
                    self.V[node] = dx*dy/2
                    self.NSEW[node, :] = [node+1, node-1, node+ny, None]
                    self.t_NSEW[node, :] = [dy, dy , dx, None]
                    self.A_NSEW[node, :] = [dx/2, dx/2, dy, dy]

                elif i==nx-1:
                    self.E.append(node)
                    self.V[node] = dx*dy/2
                    self.NSEW[node, :] = [node+1, node-1, None, node-ny]
                    self.t_NSEW[node, :] = [dy, dy , None, dx]
                    self.A_NSEW[node, :] = [dx/2, dx/2, dy, dy]

                elif j==0:
                    self.S.append(node)
                    self.V[node] = dx*dy/2
                    self.NSEW[node, :] = [node+1, None, node+ny, node-ny]
                    self.t_NSEW[node, :] = [dy, None , dx, dx]
                    self.A_NSEW[node, :] = [dx, dx, dy/2, dy/2]

                elif j==ny-1:
                    self.N.append(node)
                    self.V[node] = dx*dy/2
                    self.NSEW[node, :] = [None, node-1, node+ny, node-ny]
                    self.t_NSEW[node, :] = [None, dy, dx, dx]
                    self.A_NSEW[node, :] = [dx, dx, dy/2, dy/2]

                else:
                    self.V[node] = dx*dy
                    self.NSEW[node, :] = [node+1, node-1, node+ny, node-ny]
                    self.t_NSEW[node, :] = [dy, dy, dx, dx]
                    self.A_NSEW[node, :] = [dx, dx, dy, dy]

                node += 1
        print("Mesh Created")

