import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
from matplotlib.widgets import Slider, Button, RadioButtons


# results = CFDClasses.ReadOutputFile("Test_output.txt")
X, Y = np.mgrid[:2*np.pi:10j,:2*np.pi:5j]
U = np.cos(X)
V = np.sin(Y)

fig, ax = plt.subplots(1,1)
Q = ax.quiver(X, Y, U, V, pivot='mid', color='r', units='inches')

ax.set_xlim(-1, 7)
ax.set_ylim(-1, 7)


ax_t = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor='lightgoldenrodyellow')

t_slider = Slider(ax_t, 't (s)', 0, 100, valinit=0)

def update(val):
    # t = t_slider.val
    # index = get_index(results.t, t)
    # p.set_UVC(results.ux[index, :], results.uy[index, :])
    U = np.cos(X + val*0.1)
    V = np.sin(Y + val*0.1)

    Q.set_UVC(U,V)
    fig.canvas.draw_idle()


t_slider.on_changed(update)

plt.show()
#
# def update_quiver(num, Q, X, Y):
#     """updates the horizontal and vertical vector components by a
#     fixed increment on each frame
#     """
#
#     U = np.cos(X + num*0.1)
#     V = np.sin(Y + num*0.1)
#
#     Q.set_UVC(U,V)
#
#     return Q,
#
# # you need to set blit=False, or the first set of arrows never gets
# # cleared on subsequent frames
# anim = animation.FuncAnimation(fig, update_quiver, fargs=(Q, X, Y),
#                                interval=50, blit=False)
# fig.tight_layout()
# plt.show()