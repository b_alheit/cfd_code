from matplotlib import pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
import CFDClasses
import numpy as np


results = CFDClasses.ReadOutputFile("fastV.txt")

scale = 10

fig, ax = plt.subplots(1, 1)
plt.subplots_adjust(left=0.15, bottom=0.15)
x_pos = results.x_mesh
y_pos = results.y_mesh
u_int = results.u_mag(0)
x_col = results.x_mesh[:, 0]
y_col = results.y_mesh[0, :]
Col = ax.pcolor(x_col, y_col, results.u_mag(0)[:-1, :-1], vmin=0,
                vmax=np.max(np.sqrt(np.power(results.ux, 2) + np.power(results.uy, 2))), cmap='jet')
fig.colorbar(Col)

Q = ax.quiver(results.x_mesh, results.y_mesh, results.ux_mesh(0), results.uy_mesh(0), pivot='mid', color='black', units='width')

plt.title("Lid driven cavity velocity vector plot")
plt.xlabel('x (m)')
plt.ylabel('y (m)')
ax_t = plt.axes([0.1, 0.01, 0.75, 0.03], facecolor='lightgoldenrodyellow')

t_slider = Slider(ax_t, 't (s)', results.t[0], results.t[-1], valinit=results.t[0])


def get_index(arr, val):
    for i in range(len(arr)):
        if arr[i] >= val:
            return i

def update(val):
    t = t_slider.val
    index = get_index(results.t, t)
    # Q.set_UVC(results.ux_mesh(index), results.uy_mesh(index))
    x_dir, y_dir = results.u_dir(index)
    x_dir /= results.nx/scale
    y_dir /= results.nx/scale
    Q.set_UVC(x_dir, y_dir)
    # Col.set_array(results.u_mag(index)[1:, 1:].T.flatten())
    Col.set_array(results.u_mag_av(index).T.flatten())
    fig.canvas.draw_idle()


t_slider.on_changed(update)

plt.show()
#
# def update_quiver(num, Q, X, Y):
#     """updates the horizontal and vertical vector components by a
#     fixed increment on each frame
#     """
#
#     U = np.cos(X + num*0.1)
#     V = np.sin(Y + num*0.1)
#
#     Q.set_UVC(U,V)
#
#     return Q,
#
# # you need to set blit=False, or the first set of arrows never gets
# # cleared on subsequent frames
# anim = animation.FuncAnimation(fig, update_quiver, fargs=(Q, X, Y),
#                                interval=50, blit=False)
# fig.tight_layout()
# plt.show()