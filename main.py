import numpy as np
from matplotlib import pyplot as plt
import CFDClasses

rho = 1
mu = 0.01
p_ref = 1e5
CFL = 0.9
e = 5e-3

test_mesh = CFDClasses.Mesh(-0.5, 0.5, -0.5, 0.5, 52, 52)

test_sim = CFDClasses.CFDSimulation(rho, mu, p_ref, test_mesh, e, CFL)
test_sim.solve()
test_sim.write_output("fastV.txt")

plt.quiver(test_sim.mesh.pos[:, 0],
           test_sim.mesh.pos[:, 1],
           test_sim.u_next[:, 0],
           test_sim.u_next[:, 1])
plt.show()

plt.contour(test_sim.mesh.pos[:, 0],
             test_sim.mesh.pos[:, 1],
             test_sim.u_mag)
plt.show()


plt.plot(a, b)
plt.show()